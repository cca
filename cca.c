#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#undef WANT_SOUND
// #define WANT_SOUND

#ifdef WANT_SOUND
#include <jack/jack.h>
#include <sndfile.h>
#endif

const float pi = 3.141592653589793;

const int width = 1920;
const int height = 1080;
const int colour_width = 1920;
const int colour_height = 1080;
int mutation = 0;
int overdrive = 1;
int record = 0;
int highquality = 0;
int rt = 1;
#define FPS 25

#ifdef WANT_SOUND

#define SR 48000

typedef struct { double y; } HIP;
static inline double hip(HIP *s, double x, double hz) {
  double c = fmin(fmax(1 - 2 * pi * hz / SR, 0), 1);
  double n = 0.5 * (1 + c);
  double y = x + c * s->y;
  double o = n * (y - s->y);
  s->y = y;
  return o;
}

float audio[4][1920];
HIP filter[2];
jack_client_t *client;
jack_port_t *port[2];
SNDFILE *sndfile;
int ix;

void audiocb(float *l, float *r, int nframes) {
  float g = 1;
  for (int i = 0; i < nframes; ++i) {
    float h = i / (float) nframes;
    float h1 = 1 - h;
    l[i] = tanhf(g * hip(&filter[0], h1 * audio[0][ix] + h  * audio[2][ix], 5));
    r[i] = tanhf(g * hip(&filter[1], h1 * audio[1][ix] + h  * audio[3][ix], 5));
    ix = (ix + 1) % 1920;
  }
  memcpy(&audio[0][0], &audio[2][0], 1920 * sizeof(float));
  memcpy(&audio[1][0], &audio[3][0], 1920 * sizeof(float));
}

static volatile int incb = 0;
int processcb(jack_nframes_t nframes, void *arg) {
  (void) arg;
  jack_default_audio_sample_t *out[2];
  out[0] = jack_port_get_buffer(port[0], nframes);
  out[1] = jack_port_get_buffer(port[1], nframes);
  incb = 1;
  audiocb(out[0], out[1], nframes);
  incb = 0;
  return 0;
}

int startaudio() {
  if (rt) {
    if (! (client = jack_client_open("cca", JackNoStartServer, 0))) {
      fprintf(stderr, "jack server not running?\n");
      exit(1);
    }
    jack_set_process_callback(client, processcb, 0);
    port[0] = jack_port_register(client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    port[1] = jack_port_register(client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    if (jack_activate(client)) {
      fprintf (stderr, "cannot activate JACK client");
      exit(1);
    }
    if (jack_connect(client, "cca:output_1", "system:playback_1")) {
      fprintf(stderr, "cannot connect output port\n");
    }
    if (jack_connect(client, "cca:output_2", "system:playback_2")) {
      fprintf(stderr, "cannot connect output port\n");
    }
  } else {
    SF_INFO info = { 0, 48000, 2, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
    sndfile = sf_open("cca.wav", SFM_WRITE, &info);
  }
  return 1;
}

#endif

void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    fprintf(stderr, "%s shader compile info:\n%s\nshader source:\n%s", type_str, info ? info : "(no info log)", source ? source : "(no source)");
  }
  if (info) {
    free(info);
  }
}

GLuint vertex_fragment_shader(const char *vert, const char *frag) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &vert, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_VERTEX_SHADER, vert);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(shader, 1, &frag, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_FRAGMENT_SHADER, frag);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  debug_program(program);
  return program;
}

char *read_file(const char *name) {
  FILE *f = fopen(name, "r");
  fseek(f, 0, SEEK_END);
  long len = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *str = malloc(len + 1);
  fread(str, len, 1, f);
  str[len] = 0;
  fclose(f);
  return str;
}

struct genome {
  float blur[4];
  float speed[4];
  float decay[4];
  float coupling[4][4];
  float colour[4][4];
  float offset[4];
};

struct genome current, mutated;

void mix(struct genome *a, const struct genome *b, double p)
{
  float *x = &a->blur[0];
  const float *y = &b->blur[0];
  for (int i = 0; i < 48; ++i)
  {
    x[i] = (rand() / (double) RAND_MAX) < p ? y[i] : x[i];
  }
}

void randomize(struct genome *s)
{
#define R (rand() / (double) RAND_MAX)
  s->blur[0] = 1 + 7 * R;
  s->blur[1] = 1 + 7 * R;
  s->blur[2] = 1 + 7 * R;
  s->blur[3] = 1 + 7 * R;
  s->speed[0] = 12 * R;
  s->speed[1] = 12 * R;
  s->speed[2] = 12 * R;
  s->speed[3] = 12 * R;
  s->decay[0] = 1 - pow(2, -12 * R);
  s->decay[1] = 1 - pow(2, -12 * R);
  s->decay[2] = 1 - pow(2, -12 * R);
  s->decay[3] = 1 - pow(2, -12 * R);
  for (int i = 0; i < 4; ++i)
    for (int j = 0; j < 4; ++j)
      s->coupling[i][j] = (i == j ? 2 : 1) * (2 * R - 1);
  for (int i = 0; i < 4; ++i)
    for (int j = 0; j < 4; ++j)
      s->colour[i][j] = 2 * R - 1;
  for (int i = 0; i < 4; ++i)
    s->offset[i] = R;
#undef R
}

void reseed()
{
  int count = width * height * 4;
  float *noise = malloc(count * sizeof(float));
  for (int j = 0; j < 4; ++j)
  {
    for (int i = 0; i < count; ++i)
      noise[i] = (j < 2) * rand() / (double) RAND_MAX;
    glActiveTexture(GL_TEXTURE0 + j);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, width, height, 4, 0, GL_RED, GL_FLOAT, noise);
    if (j < 2)
      glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
  }
  free(noise);
}

void print_parameters(const struct genome *s)
{
  fprintf(stderr, "%.8f", s->blur[0]);
  for (int i = 1; i < 4; ++i)
    fprintf(stderr, " %.8f", s->blur[i]);
  fprintf(stderr, " /");
  for (int i = 0; i < 4; ++i)
    fprintf(stderr, " %.8f", s->speed[i]);
  fprintf(stderr, " /");
  for (int i = 0; i < 4; ++i)
    fprintf(stderr, " %.8f", s->decay[i]);
  for (int i = 0; i < 4; ++i)
  {
    fprintf(stderr, " /");
    for (int j = 0; j < 4; ++j)
      fprintf(stderr, " %.8f", s->coupling[i][j]);
  }
  for (int i = 0; i < 4; ++i)
  {
    fprintf(stderr, " /");
    for (int j = 0; j < 4; ++j)
      fprintf(stderr, " %.8f", s->colour[i][j]);
  }
  fprintf(stderr, " /");
  for (int i = 0; i < 4; ++i)
    fprintf(stderr, " %.8f", s->offset[i]);
  fprintf(stderr, "\n");
}

int parse_parameters(struct genome *s)
{
  if (48 != scanf(
    "%f %f %f %f / %f %f %f %f / %f %f %f %f / "
    "%f %f %f %f / %f %f %f %f / %f %f %f %f / %f %f %f %f / "
    "%f %f %f %f / %f %f %f %f / %f %f %f %f / %f %f %f %f / "
    "%f %f %f %f",
    &s->blur[0], &s->blur[1], &s->blur[2], &s->blur[3],
    &s->speed[0], &s->speed[1], &s->speed[2], &s->speed[3],
    &s->decay[0], &s->decay[1], &s->decay[2], &s->decay[3],
    &s->coupling[0][0], &s->coupling[0][1], &s->coupling[0][2], &s->coupling[0][3],
    &s->coupling[1][0], &s->coupling[1][1], &s->coupling[1][2], &s->coupling[1][3],
    &s->coupling[2][0], &s->coupling[2][1], &s->coupling[2][2], &s->coupling[2][3],
    &s->coupling[3][0], &s->coupling[3][1], &s->coupling[3][2], &s->coupling[3][3],
    &s->colour[0][0], &s->colour[0][1], &s->colour[0][2], &s->colour[0][3],
    &s->colour[1][0], &s->colour[1][1], &s->colour[1][2], &s->colour[1][3],
    &s->colour[2][0], &s->colour[2][1], &s->colour[2][2], &s->colour[2][3],
    &s->colour[3][0], &s->colour[3][1], &s->colour[3][2], &s->colour[3][3],
    &s->offset[0], &s->offset[1], &s->offset[2], &s->offset[3]
  ))
  {
    fprintf(stderr, "cca: error: parse failed\n");
    return 0;
  }
  return 1;
}

void keycb(GLFWwindow *window, int key, int scancode, int action, int mods) {
  (void) scancode;
  (void) mods;
  if (action == GLFW_PRESS) {
    switch (key) {
      case GLFW_KEY_Q:
      case GLFW_KEY_ESCAPE:
        glfwSetWindowShouldClose(window, GL_TRUE);
        break;
      case GLFW_KEY_SPACE:
        randomize(&current);
        break;
      case GLFW_KEY_ENTER:
        reseed();
        break;
      case GLFW_KEY_O:
        overdrive = 256 / overdrive;
        fprintf(stderr, "overdrive %s\n", overdrive == 1 ? "disabled" : "enabled");
        break;
      case GLFW_KEY_M:
        mutation = 1 - mutation;
        fprintf(stderr, "mutation %s\n", mutation == 0 ? "disabled" : "enabled");
        break;
      case GLFW_KEY_H:
        highquality = 1 - highquality;
        fprintf(stderr, "high quality %s\n", highquality == 0 ? "disabled" : "enabled");
        break;
      case GLFW_KEY_P:
        print_parameters(&current);
        break;
      case GLFW_KEY_L:
        parse_parameters(&current);
        break;
      case GLFW_KEY_R:
        record += 250 * overdrive;
        break;
      case GLFW_KEY_S:
        record += overdrive;
        break;
    }
  }
}

int main()
{
  srand(time(0));

  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
//  glfwWindowHint(GLFW_DECORATED, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(width, height, "Coupled Cellular Automata", 0, 0);
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew 
  glfwSetKeyCallback(window, keycb);

  GLuint tex[5];
  glGenTextures(5, &tex[0]);
  int count = width * height * 4;
  float *image = malloc(count * sizeof(float));
  for (int j = 0; j < 5; ++j)
  {
    for (int i = 0; i < count; ++i) image[i] = rand() / (double) RAND_MAX;
    glActiveTexture(GL_TEXTURE0 + j);
    glBindTexture(GL_TEXTURE_2D_ARRAY, tex[j]);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, width, height, 4, 0, GL_RED, GL_FLOAT, image);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, j < 2 ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, j < 2 ? GL_LINEAR : GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
  }

  // temporary texture for blurring
  GLuint btex;
  glGenTextures(1, &btex);
  glActiveTexture(GL_TEXTURE0 + 5);
  glBindTexture(GL_TEXTURE_2D, btex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, image);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  // Gaussian kernel
  float kernel[256];
  for (int i = 0; i < 256; ++i)
  {
    float x = 3.0f * ((i + 0.5f) / 256.0f - 0.5f);
    kernel[i] = expf(-x * x);
  }
  GLuint ktex;
  glGenTextures(1, &ktex);
  glActiveTexture(GL_TEXTURE0 + 6);
  glBindTexture(GL_TEXTURE_1D, ktex);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, 256, 0, GL_RED, GL_FLOAT, &kernel[0]);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glGenerateMipmap(GL_TEXTURE_1D);

  GLenum status;

  // framebuffer for blurring
  GLuint fbob[5];
  glGenFramebuffers(5, &fbob[0]);
  GLenum buffersb = GL_COLOR_ATTACHMENT0;
  glBindFramebuffer(GL_FRAMEBUFFER, fbob[4]);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, btex, 0);
  glDrawBuffers(1, &buffersb);
  status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) fprintf(stderr, "FBO ERR %d\n", status);
  for (int i = 0; i < 4; ++i)
  {
    glBindFramebuffer(GL_FRAMEBUFFER, fbob[i]);
    glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex[4], 0, i);
    glDrawBuffers(1, &buffersb);
    status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) fprintf(stderr, "FBO ERR %d\n", status);
  }
  // framebuffers for ping pong rendering
  GLuint fbo8[2];
  glGenFramebuffers(2, &fbo8[0]);
  GLenum buffers8[8] =
    { GL_COLOR_ATTACHMENT0
    , GL_COLOR_ATTACHMENT1
    , GL_COLOR_ATTACHMENT2
    , GL_COLOR_ATTACHMENT3
    , GL_COLOR_ATTACHMENT4
    , GL_COLOR_ATTACHMENT5
    , GL_COLOR_ATTACHMENT6
    , GL_COLOR_ATTACHMENT7
    };
  // ping
  glBindFramebuffer(GL_FRAMEBUFFER, fbo8[0]);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex[1], 0, 0);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, tex[1], 0, 1);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, tex[1], 0, 2);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, tex[1], 0, 3);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, tex[3], 0, 0);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, tex[3], 0, 1);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT6, tex[3], 0, 2);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT7, tex[3], 0, 3);
  glDrawBuffers(8, buffers8);
  status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) fprintf(stderr, "FBO ERR %d\n", status);
  // pong
  glBindFramebuffer(GL_FRAMEBUFFER, fbo8[1]);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex[0], 0, 0);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, tex[0], 0, 1);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, tex[0], 0, 2);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, tex[0], 0, 3);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, tex[2], 0, 0);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, tex[2], 0, 1);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT6, tex[2], 0, 2);
  glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT7, tex[2], 0, 3);
  glDrawBuffers(8, buffers8);
  status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) fprintf(stderr, "FBO ERR %d\n", status);

  // texture and framebuffer for colouring
  GLuint ctex;
  glGenTextures(1, &ctex);
  int ccount = colour_width * colour_height * 4;
  unsigned char *cimage = malloc(ccount);
  for (int i = 0; i < ccount; ++i)
    cimage[i] = 255;
  glActiveTexture(GL_TEXTURE0 + 7);
  glBindTexture(GL_TEXTURE_2D, ctex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, colour_width, colour_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, cimage);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, colour_width != width || colour_height != height ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, colour_width != width || colour_height != height ? GL_LINEAR : GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  if (colour_width != width || colour_height != height)
    glGenerateMipmap(GL_TEXTURE_2D);
  GLuint fboc;
  glGenFramebuffers(1, &fboc);
  GLenum buffersc = GL_COLOR_ATTACHMENT0;
  glBindFramebuffer(GL_FRAMEBUFFER, fboc);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ctex, 0);
  glDrawBuffers(1, &buffersc);
  status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) fprintf(stderr, "FBO ERR %d\n", status);

  char *vert = read_file("cca_vert.glsl");
  char *frag = read_file("cca_frag.glsl");
  GLuint p_update = vertex_fragment_shader(vert, frag);
  free(frag);
  GLint u_blur     = glGetUniformLocation(p_update, "blur");
  GLint u_speed    = glGetUniformLocation(p_update, "speed");
  GLint u_decay    = glGetUniformLocation(p_update, "decay");
  GLint u_coupling = glGetUniformLocation(p_update, "coupling");
  GLint u_state    = glGetUniformLocation(p_update, "state");
  GLint u_history  = glGetUniformLocation(p_update, "history");
  GLint u_blurred  = glGetUniformLocation(p_update, "blurred");
  GLint u_highquality = glGetUniformLocation(p_update, "highquality");
  glUseProgram(p_update);
  glUniform1i(u_blurred, 4);
  frag = read_file("colour_frag.glsl");
  GLuint p_colour = vertex_fragment_shader(vert, frag);
  free(frag);
  GLint u_cstate  = glGetUniformLocation(p_colour, "state");
  GLint u_ccolour = glGetUniformLocation(p_colour, "colour");
  GLint u_coffset = glGetUniformLocation(p_colour, "offset");
  frag = read_file("blurx_frag.glsl");
  GLuint p_blurx = vertex_fragment_shader(vert, frag);
  free(frag);
  GLuint u_kernelx = glGetUniformLocation(p_blurx, "kernel");
  GLuint u_sourcex = glGetUniformLocation(p_blurx, "source");
  GLuint u_layerx  = glGetUniformLocation(p_blurx, "layer");
  GLuint u_blurx   = glGetUniformLocation(p_blurx, "blur");
  glUseProgram(p_blurx);
  glUniform1i(u_kernelx, 6);
  frag = read_file("blury_frag.glsl");
  GLuint p_blury = vertex_fragment_shader(vert, frag);
  free(frag);
  GLuint u_kernely = glGetUniformLocation(p_blury, "kernel");
  GLuint u_sourcey = glGetUniformLocation(p_blury, "source");
  GLuint u_blury   = glGetUniformLocation(p_blury, "blur");
  glUseProgram(p_blury);
  glUniform1i(u_kernely, 6);
  glUniform1i(u_sourcey, 5);
  frag = read_file("display_frag.glsl");
  GLuint p_display = vertex_fragment_shader(vert, frag);
  free(frag);
  GLint u_dtex = glGetUniformLocation(p_display, "tex");
  glUseProgram(p_display);
  glUniform1i(u_dtex, 7);
  free(vert);

  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  glViewport(0, 0, width, height);

#ifdef WANT_SOUND
  startaudio();
#endif

  int which = 0;
  randomize(&current);
  reseed();
  int frame = 0;
  while (! glfwWindowShouldClose(window))
  {
    glfwPollEvents();
    
    glUseProgram(p_update);
    glUniform4fv(u_blur,  1, &current.blur [0]);
    glUniform4fv(u_speed, 1, &current.speed[0]);
    glUniform4fv(u_decay, 1, &current.decay[0]);
    glUniformMatrix4fv(u_coupling, 1, GL_FALSE, &current.coupling[0][0]);
    for (int o = 0; o < overdrive; ++o)
    {
      if (mutation)
      {
        randomize(&mutated);
        mix(&current, &mutated, 0.001);
      }
      if (highquality)
        for (int i = 0; i < 4; ++i)
        {
          glUseProgram(p_blurx);
          glUniform1i(u_sourcex, which);
          glUniform1i(u_layerx, i);
          glUniform1f(u_blurx, current.blur[i]);
          glBindFramebuffer(GL_FRAMEBUFFER, fbob[4]);
          glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
          glUseProgram(p_blury);
          glUniform1f(u_blury, current.blur[i]);
          glBindFramebuffer(GL_FRAMEBUFFER, fbob[i]);
          glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        }
      glUseProgram(p_update);
      glUniform1i(u_highquality, highquality);
      glUniform1i(u_state, which);
      glUniform1i(u_history, which + 2);
      glBindFramebuffer(GL_FRAMEBUFFER, fbo8[which]);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
      which = 1 - which;
      glActiveTexture(GL_TEXTURE0 + which);
      glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
    }

#ifdef WANT_SOUND
    if (rt)
      while (incb)
        ;
    glReadPixels(0, 270, 1920, 1, GL_RED, GL_FLOAT, &audio[2][0]);
    glReadPixels(0, 810, 1920, 1, GL_RED, GL_FLOAT, &audio[3][0]);
    if (! rt)
    {
      float audioo[2][SR/FPS];
      audiocb(&audioo[0][0], &audioo[1][0], SR/FPS);
      float frames[SR/FPS][2];
      for (int i = 0; i < SR/FPS; ++i)
        for (int c = 0; c < 2; ++c)
          frames[i][c] = audioo[c][i];
      sf_writef_float(sndfile, &frames[0][0], SR/FPS);
    }
#endif

    glUseProgram(p_colour);
    glUniform1i(u_cstate, which);
    glUniformMatrix4fv(u_ccolour, 1, GL_FALSE, &current.colour[0][0]);
    glUniform4fv(u_coffset, 1, &current.offset[0]);
    glBindFramebuffer(GL_FRAMEBUFFER, fboc);
    glViewport(0, 0, colour_width, colour_height);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glActiveTexture(GL_TEXTURE0 + 7);
    if (colour_width != width || colour_height != height)
      glGenerateMipmap(GL_TEXTURE_2D);
    if (record > 0)
    {
      glReadPixels(0, 0, colour_width, colour_height, GL_RGB, GL_UNSIGNED_BYTE, cimage);
      fprintf(stdout, "P6\n%d %d\n255\n", colour_width, colour_height);
      fwrite(cimage, colour_width * colour_height * 3, 1, stdout);
      fflush(stdout);
      record--;
    }
    glUseProgram(p_display);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, width, height);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glfwSwapBuffers(window);
    int e = glGetError();
    if (e)
      fprintf(stderr, "%d\n", e);
    frame++;
    if (! rt && frame >= FPS * 300) {
#ifdef WANT_SOUND
      sf_close(sndfile);
#endif
      break;
    }
  }
  fprintf(stderr, "%d frames rendered\n", frame);
  glfwTerminate();
  return 0;
}

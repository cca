#version 330 core

uniform sampler2DArray source;
uniform sampler1D kernel;

uniform int layer;
uniform float blur;

in vec2 coord;

layout(location = 0) out float blurred;

void main()
{
  ivec2 s = textureSize(source, 0).xy;
  ivec2 p = ivec2(floor(vec2(s) * coord));
  float r = pow(2.0, blur);
  int k = int(ceil(r));
  vec2 b = vec2(0.0);
  for (int i = -k; i <= k; ++i)
  {
    float x = 0.5 + 0.5 * float(i)/r;
    float w = texture(kernel, x).x;
    b += w * vec2(texelFetch(source, ivec3((p.x + i + s.x) % s.x, p.y, layer), 0).x, 1.0);
  }
  blurred = b.x / b.y;
}

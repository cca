#!/bin/bash
kill -s SIGSTOP "${@}"
running=0
stop_threshold=85
cont_threshold=75
while true
do
  temperature="$(nvidia-smi -q -d TEMPERATURE | grep 'GPU Current Temp' | sed 's/^.*: \(.*\) C$/\1/')"
  if (( running ))
  then
    if (( temperature > stop_threshold ))
    then
      echo "STOP ${temperature} > ${stop_threshold}"
      kill -s SIGSTOP "${@}"
      running=0
    fi
  else
    if (( temperature < cont_threshold ))
    then
      echo "CONT ${temperature} < ${cont_threshold}"
      kill -s SIGCONT "${@}"
      running=1
    fi
  fi
  sleep 1
done |
ts

#version 330 core

uniform sampler2D tex;

in vec2 coord;

out vec4 colour_out;

void main()
{
  colour_out = texture(tex, coord);
}

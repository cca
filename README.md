Coupled Cellular Automata
=========================

Usage
-----

Needs GLEW and GLFW.  Type `make` to build, `./cca` to run.


Keyboard Controls
-----------------

ESC, Q   -- quit
SPACE    -- randomize parameters
ENTER    -- randomize state
P        -- print parameters to stderr
L        -- load parameters from stdin
M        -- toggle mutation
O        -- toggle overdrive (faster calculations)
H        -- toggle high quality (slower but prettier)
R        -- PPM recording (to stdout, so redirect it!)
S        -- PPM screenshot (to stdout, so redirect it!)

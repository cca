#version 330 core

uniform sampler2DArray state;

uniform mat4 colour;
uniform vec4 offset;

in vec2 coord;

out vec4 colour_out;

float cubic_interp(float t, vec4 a)
{
  const mat4 m = mat4(0.0,2.0,0.0,0.0, -1.0,0.0,1.0,0.0, 2.0,-5.0,4.0,-1.0, -1.0,3.0,-3.0,1.0);
  vec4 f = vec4(1.0, t, t*t, t*t*t);
  return dot(0.5 * f * transpose(m), a);
}

float cubic_row(sampler2DArray tex, ivec2 p, int layer, int lod, float t)
{
  ivec2 s = textureSize(tex, lod).xy;
  float a = texelFetch(tex, ivec3((p + ivec2(s.x - 1, 0)) % s, layer), lod).x;
  float b = texelFetch(tex, ivec3( p                         , layer), lod).x;
  float c = texelFetch(tex, ivec3((p + ivec2(1      , 0)) % s, layer), lod).x;
  float d = texelFetch(tex, ivec3((p + ivec2(2      , 0)) % s, layer), lod).x;
  return cubic_interp(t, vec4(a, b, c, d));
}

float cubic(sampler2DArray tex, vec2 p, int layer, int lod)
{
  ivec2 s = textureSize(tex, lod).xy;
  vec2 q = mod(vec2(s) * (p + vec2(1.0)) - vec2(0.5), s);
  ivec2 r = ivec2(floor(q));
  vec2 t = q - vec2(r);
  float a = cubic_row(tex, (r + ivec2(0, s.y - 1)) % s, layer, lod, t.x);
  float b = cubic_row(tex,  r                         , layer, lod, t.x);
  float c = cubic_row(tex, (r + ivec2(0, 1      )) % s, layer, lod, t.x);
  float d = cubic_row(tex, (r + ivec2(0, 2      )) % s, layer, lod, t.x);
  return cubic_interp(t.y, vec4(a, b, c, d));
}

float cubic_mipmap_linear(sampler2DArray tex, vec2 p, int layer, float lod)
{
  int lod0 = int(floor(lod));
  int lod1 = lod0 + 1;
  float t = lod - float(lod0);
  return mix(cubic(tex, p, layer, lod0), cubic(tex, p, layer, lod1), t);
}

void main()
{
  vec2 dx = dFdx(coord);
  vec2 dy = dFdy(coord);
  vec4 s = vec4
    ( texture(state, vec3(coord, 0.0), 100.0).x
    , texture(state, vec3(coord, 1.0), 100.0).x
    , texture(state, vec3(coord, 2.0), 100.0).x
    , texture(state, vec3(coord, 3.0), 100.0).x
    );
  vec4 s00 = vec4
    ( texture(state, vec3(coord, 0.0)).x
    , texture(state, vec3(coord, 1.0)).x
    , texture(state, vec3(coord, 2.0)).x
    , texture(state, vec3(coord, 3.0)).x
    );
  vec4 s01 = vec4
    ( texture(state, vec3(coord + dy, 0.0)).x
    , texture(state, vec3(coord + dy, 1.0)).x
    , texture(state, vec3(coord + dy, 2.0)).x
    , texture(state, vec3(coord + dy, 3.0)).x
    );
  vec4 s10 = vec4
    ( texture(state, vec3(coord + dx, 0.0)).x
    , texture(state, vec3(coord + dx, 1.0)).x
    , texture(state, vec3(coord + dx, 2.0)).x
    , texture(state, vec3(coord + dx, 3.0)).x
    );
  vec4 s11 = vec4
    ( texture(state, vec3(coord + dx + dy, 0.0)).x
    , texture(state, vec3(coord + dx + dy, 1.0)).x
    , texture(state, vec3(coord + dx + dy, 2.0)).x
    , texture(state, vec3(coord + dx + dy, 3.0)).x
    );
  vec4 c00 = colour * (s00 - s) + offset;
  vec4 c01 = colour * (s01 - s) + offset;
  vec4 c10 = colour * (s10 - s) + offset;
  vec4 c11 = colour * (s11 - s) + offset;
  vec4 c = 0.25 * (c00 + c01 + c10 + c11);
  float e = clamp(8.0 * length(vec2(length(c00 - c11), length(c10 - c01))), 0.0, 1.0);
  colour_out = mix(vec4(1.0), clamp(c, 0.0, 1.0), e);
}

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <glm/glm.hpp>

struct pixel
{
  int n;
  unsigned char x[4];
};

int compare_r(const void *a, const void *b)
{
  int x = ((const struct pixel *) a)->x[0];
  int y = ((const struct pixel *) b)->x[0];
  return (x > y) - (y > x);
}

int compare_g(const void *a, const void *b)
{
  int x = ((const struct pixel *) a)->x[1];
  int y = ((const struct pixel *) b)->x[1];
  return (x > y) - (y > x);
}

int compare_b(const void *a, const void *b)
{
  int x = ((const struct pixel *) a)->x[2];
  int y = ((const struct pixel *) b)->x[2];
  return (x > y) - (y > x);
}

int compare_n(const void *a, const void *b)
{
  int x = ((const struct pixel *) a)->n;
  int y = ((const struct pixel *) b)->n;
  return (x > y) - (y > x);
}

void median_cut(struct pixel *xs, int count, int depth)
{
  if (depth == 0)
  {
    // compute average of box
    double r = 0, g = 0, b = 0;
    for (int i = 0; i < count; ++i)
    {
      r += xs[i].x[0];
      g += xs[i].x[1];
      b += xs[i].x[2];
    }
    unsigned char rc = r / count;
    unsigned char gc = g / count;
    unsigned char bc = b / count;    
    // set box pixels to colour
    for (int i = 0; i < count; ++i)
    {
      xs[i].x[0] = rc;
      xs[i].x[1] = gc;
      xs[i].x[2] = bc;
    }
  }
  else
  {
    // compute axis range of box
    struct pixel lo = { -1, { 255, 255, 255 } };
    struct pixel hi = { -1, { 0, 0, 0 } };
    for (int i = 0; i < count; ++i)
    {
      lo.x[0] = std::min(lo.x[0], xs[i].x[0]);
      lo.x[1] = std::min(lo.x[1], xs[i].x[1]);
      lo.x[2] = std::min(lo.x[2], xs[i].x[2]);
      hi.x[0] = std::max(hi.x[0], xs[i].x[0]);
      hi.x[1] = std::max(hi.x[1], xs[i].x[1]);
      hi.x[2] = std::max(hi.x[2], xs[i].x[2]);
    }
    // compute longest dimension
    int dim = -1;
    int range = -1;
    for (int c = 0; c < 3; ++c)
    {
      if (hi.x[c] - lo.x[c] > range)
      {
        range = hi.x[c] - lo.x[c];
        dim = c;
      }
    }
    // sort along longest dimension
    comparison_fn_t compare_rgbs[3] = { compare_r, compare_g, compare_b };
    std::qsort(xs, count, sizeof(*xs), compare_rgbs[dim]);
    // subdivide at midpoint and recurse
    int median = count / 2;
    unsigned char median_value = xs[median].x[dim];
    while (xs[median].x[dim] == median_value && median < count)
    {
      median++;
    }
    if (0 < median)
    {
      median_cut(xs, median, depth - 1);
    }
    if (median < count)
    {
      median_cut(xs + median, count - median, depth - 1);
    }
  }
}

void main1(unsigned char *raw, int count)
{
  struct pixel *pixels = (struct pixel *) std::malloc(sizeof(struct pixel) * count);
  for (int i = 0; i < count; ++i)
  {
    pixels[i].n = i;
    pixels[i].x[0] = raw[3 * i + 0];
    pixels[i].x[1] = raw[3 * i + 1];
    pixels[i].x[2] = raw[3 * i + 2];
  }
  median_cut(pixels, count, 3);
  std::qsort(pixels, count, sizeof(*pixels), compare_n);
  for (int i = 0; i < count; ++i)
  {
    raw[3 * i + 0] = pixels[i].x[0];
    raw[3 * i + 1] = pixels[i].x[1];
    raw[3 * i + 2] = pixels[i].x[2];
  }
  std::free(pixels);
}

int main(int argc, char **argv)
{
  std::srand(std::time(0));
  for (int arg = 1; arg < argc; ++arg)
  {
    std::FILE *ppm = std::fopen(argv[arg], "rb");
    if (ppm)
    {
      if ('P' == std::fgetc(ppm))
      {
        if ('6' == std::fgetc(ppm))
        {
          if ('\n' == std::fgetc(ppm))
          {
            if ('#' == std::fgetc(ppm))
            {
              while ('\n' != std::fgetc(ppm))
              {
                if (std::feof(ppm))
                {
                  break;
                }
              }
            }
            else
            {
              std::fprintf(stderr, "# expected\n");
            }
            int width = 0, height = 0;
            if (2 == std::fscanf(ppm, "%d %d\n255", &width, &height))
            {
              if ('\n' == std::fgetc(ppm))
              {
                int bytes = width * height * 3;
                unsigned char *rgb = (unsigned char *)std::malloc(bytes);
                if (rgb)
                {
                  if (1 == std::fread(rgb, bytes, 1, ppm))
                  {
                    main1(rgb, width * height);
                    std::fprintf(stdout, "P6\n%d %d\n255\n", width, height);
                    std::fwrite(rgb, bytes, 1, stdout);
                    std::fflush(stdout);
                  }
                  else
                  {
                    std::fprintf(stderr, "couldn't read %d bytes\n", bytes);
                  }
                  std::free(rgb);
                }
                else
                {
                  std::fprintf(stderr, "couldn't allocate %d bytes\n", bytes);
                }
              }
              else
              {
                std::fprintf(stderr, "newline expected after dims\n");
              }
            }
            else
            {
              std::fprintf(stderr, "dims expected\n");
            }
          }
          else
          {
            std::fprintf(stderr, "newline expected\n");
          }
        }
        else
        {
          std::fprintf(stderr, "6 expected\n");
        }
      }
      else
      {
        std::fprintf(stderr, "P expected\n");
      }
      std::fclose(ppm);
    }
  }
  return 0;
}

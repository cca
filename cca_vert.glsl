#version 330 core

out vec2 coord;

void main()
{
  float x = float(gl_VertexID & 1);
  float y = float(gl_VertexID & 2) / 2.0;
  vec2 p = vec2(x, y);
  gl_Position = vec4(2.0 * p - vec2(1.0), 0.0, 1.0);
  coord = p;
}

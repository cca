#version 330 core

uniform sampler2DArray state;
uniform sampler2DArray history;
uniform sampler2DArray blurred;

uniform bool highquality;
uniform vec4 blur;
uniform mat4 coupling;
uniform vec4 speed;
uniform vec4 decay;

in vec2 coord;

layout(location = 0) out float state_out0;
layout(location = 1) out float state_out1;
layout(location = 2) out float state_out2;
layout(location = 3) out float state_out3;
layout(location = 4) out float history_out0;
layout(location = 5) out float history_out1;
layout(location = 6) out float history_out2;
layout(location = 7) out float history_out3;

void main()
{
  vec4 s1, s100, s, h;
  for (int k = 0; k < 4; ++k)
  {
    vec3 p = vec3(coord, float(k));
    s1[k] = texture(state, p, 1.0).x;
    s100[k] = texture(state, p, 100.0).x;
    if (highquality)
      s[k] = texture(blurred, p).x;
    else
      s[k] = texture(state, p, blur[k]).x;
    h[k] = texture(history, p).x;
  }
  s = coupling * (s - s100) + h;
  s = speed * s;
  s = mix(s1, vec4(0.5) + 0.5 * cos(s), 0.125);
  h = mix(s, h, decay);
  state_out0 = s[0];
  state_out1 = s[1];
  state_out2 = s[2];
  state_out3 = s[3];
  history_out0 = h[0];
  history_out1 = h[1];
  history_out2 = h[2];
  history_out3 = h[3];
}
